/**
 * File:    flood.c
 * Author:  Jan Remes (jan.remes@hig.no)
 * Project: CoffeeCode
 *
 * Description: Calculate rectangular flooding
 */

#include <stdio.h>
#include <stdlib.h>

#define ERR(msg) do { \
    fprintf(stderr, "ERROR: %s\n", (msg)); \
    exit(EXIT_FAILURE); \
} while(0)


/**
 * @brief   Floods current tile and calls itself on lower neighbors
 * @param[in]   input   Landscape map
 * @param[out]  output  Flooding map
 * @param[in]   width   Maps' width
 * @param[in]   height  Maps' height
 * @param[in]   x       Current tile X coordinate
 * @param[in]   y       Current tile Y coordinate
 */
void flood_recursive(int input[], int output[], int width, int height, int x, int y) {
    output[y * width + x] = 1;

    // check for "out-of-map" and call yourself on left neighbor
    if(x > 0 && input[y * width + x-1] < input[y * width + x]) {
        flood_recursive(input, output, width, height, x-1, y);
    }

    // check for "out-of-map" and call yourself on right neighbor
    if(x < width - 1 && input[y * width + x+1] < input[y * width + x]) {
        flood_recursive(input, output, width, height, x+1, y);
    }

    // check for "out-of-map" and call yourself on top neighbor
    if(y > 0 && input[(y-1) * width + x] < input[y * width + x]) {
        flood_recursive(input, output, width, height, x, y-1);
    }

    // check for "out-of-map" and call yourself on bottom neighbor
    if(y < height - 1 && input[(y+1) * width + x] < input[y * width + x]) {
        flood_recursive(input, output, width, height, x, y+1);
    }
}


/**
 * @brief   Read landscape map and flood it
 * @param[in]   landscape   Landscape map file stream (must be fopen()'d)
 * @param[in]   width       Width of the map
 * @param[in]   height      Height of the map
 * @param[in]   start_x     X coordinate of the flood start
 * @param[in]   start_y     Y coordinate of the flood start
 *
 * Expects the file heder to be already read and consumed. Will consume the rest
 * of the file, run the recursive flooding algorithm and produce the output
 */
void process_further(FILE* landscape, int width, int height, int start_x, int start_y) {
    int input[height * width];
    int output[height * width];

    // Read landscape map
    for(int i = 0; i < height; i++) {
        for(int j = 0; j < width; j++) {
            if(fscanf(landscape, " %d", &(input[i * width + j])) != 1) {
                ERR("Reading file body failed");
            }
        }
    }

    // Initialize output - nothing is flooded
    for(int i = 0; i < height; i++) {
        for (int j = 0; j < width; j++) {
            output[i * width + j] = 0;
        }
    }

    // Run recursive flooding algorithm to flood the map
    flood_recursive(input, output, width, height, start_x, start_y);

    // Print out the flooding map in required format
    printf("%d %d\n", width, height);
    for(int i = 0; i < height; i++) {
        for(int j = 0; j < width; j++) {
            if(j) {
                putchar('\t');
            }
            printf("%d", output[i * width + j]);
        }
        putchar('\n');
    }
}

/**
 * @brief   Check params, open landscape map file, parse header and process it
 * @param[in]   argc    Argument count
 * @param[in]   argv    Argument array
 * @return Program return value
 */
int main (int argc, char *argv[]) {
    int x, y;
    int width,height;
    FILE* landscape;

    if(argc != 4) {
        ERR("Wrong number of arguments, 3 required");
    }

    if(sscanf(argv[2], "%d", &x) != 1) {
        ERR("Parsing X coordinate failed");
    }

    if(sscanf(argv[3], " %d", &y) != 1) {
        ERR("Parsing Y coordinate failed");
    }

    landscape = fopen(argv[1], "r");

    if(!landscape) {
        ERR("Unable to open the landscape file");
    }

    if(fscanf(landscape, "%d %d", &width, &height) != 2) {
        ERR("Reading file header failed");
    }

    process_further(landscape, width, height, x, y);

    fclose(landscape);

    return(EXIT_SUCCESS);
}
