#!/bin/bash

# Generates landscape map files
# Usage: ./generator.sh WIDTH HEIGHT

# Maps are generated to the stdout

echo $1 $2
 for i in $(seq 1 $2); do
     for j in $(seq 1 $1); do
         if [ $j -gt 1 ]; then
             echo -ne "\t"
         fi
         let num=RANDOM/1000
         echo -n $num
     done
     echo ""
 done
