#!/bin/bash

for i in {1..10}; do
    ./generator.sh 42 157 >> testfile.txt
    ./$1 testfile.txt 28 49 >> tested
    ./flood testfile.txt 28 49 >> reference

    mydiff=$(diff tested reference)

    if [ -z "$mydiff" ]; then
        echo -e "Test $i ... \t [ OK ]"
    else
        echo -e "Test $i ... \t [FAIL]"
        cp testfile.txt testfile_$i.txt
        cp tested wrong_$i.txt
        cp reference right_$i.txt
    fi
done

rm -f testfile.txt tested reference
