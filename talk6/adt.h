/**
 * File:	adt.h
 * Author:	Honza Remes (jan.remes@hig.no)
 * Project:	CoffeeCode
 *
 * Descriptiion: Common header for the ADT library
 */
#ifndef ADT_H_INCLUDED
#define ADT_H_INCLUDED

enum ecodes {
	EOK = 0,
	ENULL,
	EINVALID,
	EMALLOC,
	EUNKNOWN = 127
};

#include "list.h"

#endif // ADT_H_INCLUDED
