/**
 * File:	list.h
 * Author:	Honza Remes (jan.remes@hig.no)
 * Project:	CoffeeCode
 *
 * Description: Provides types and function prototypes for lists in ADT library
 */

#ifndef LIST_H_INCLUDED
#define LIST_H_INCLUDED

#include "adt.h"

/* ----[ TYPES ]------------------------------------------------------------- */

typedef int listdata_t;

struct listitem;

typedef struct listitem {
	listdata_t data;
	struct listitem *prev;
	struct listitem *next;
} listitem_t;

typedef struct listheader {
	int size;
	struct listitem *first;
	struct listitem *last;
} list_t;

/* ----[ PROTOTYPES ]-------------------------------------------------------- */

/**
 * @brief	Create new empty list
 * @return	Pointer to the new list; NULL on error
 */
list_t * list_create(void);

/**
 * @brief	Destroy existing list
 * @param[out]	l	List to be destroyed
 *
 * The list must not be used after being destroyed
 */
void list_destroy(list_t * l);

/**
 * @brief	Remove all items from the list
 * @param[out]	l	List to be cleared
 * @return		0 on success, error code otherwise
 *
 * The list can be used after clearing, it will be empty
 */
int list_clear(list_t * l);

/**
 * @brief	Append new item to the specified list
 * @param[out]	l		The list to append to
 * @param[in]	item	The item to be appended
 * @return		0 on success; error code on failure
 */
int list_append(list_t * l, listdata_t item);

/**
 * @brief	Search for an item in the list
 * @param[in]	l		The list to be searched
 * @param[in]	item	The item to search for
 * @return		Pointer to the list member containing found item; NULL on error or when not found
 */
listitem_t * list_search(list_t * l, listdata_t item);

/**
 * @brief	Remove an item from the list
 * @param[out]	l		The list to remove item from
 * @param[in]	item	The item to be removed
 * @return		0 on success, error code otherwise
 */
int list_remove(list_t * l, listdata_t item);

/**
 * @brief	Get the size of a list
 * @param[in]	l	The list do get size of
 * @return		The size of the specified list; negative number on error
 */
int list_size(list_t * l);

#endif	// LIST_H_INCLUDED
