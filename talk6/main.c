/**
 * File:	main.c
 * Author:	Honza Remes (jan.remes@hig.no)
 * Project:	CoffeeCode
 *
 * Description: Uses ADT library to manage test results
 */

/**** [ INCLUDES ] ********************************************************** */
#include "adt.h"

#include <stdio.h>
#include <stdlib.h>

/**** [ DEFINES and MACROS ] ************************************************ */
#define ERR(msg) do { \
	fprintf(stderr, "ERROR: %s\n", (msg)); \
	exit(-1); \
	} while(0)

/**** [ PROTOTYPES ] ******************************************************** */
void print_help(void);
void print_results(list_t * results);


/**** [ FUNCTIONS ] ********************************************************* */
int main(void) {
	list_t * results = list_create();
	char command;
	int value;
	int running = 1;

	if(results == NULL) {
		ERR("Could not create list");
	}

	while(running) {
		printf("Input operation code and optional number [h for help]: ");

		if(scanf(" %c", &command) != 1) {
			printf("Letter not recognized");
			continue;
		}

		switch(command) {
		case 'q':
			running = 0;
			break;

		case 'h':
			print_help();
			break;

		case 'p':
			print_results(results);
			break;

		case 'a':
			if(scanf(" %d", &value) != 1) {
				printf("Added value not recognized");
				continue;
			}

			list_append(results, value);
			break;
		}
	}

	return 0;
}

void print_help(void) {
	printf("This is help... well, will be\n"
		   "Following commands are available\n"
		   "q			Quits the program\n"
		   "h			Prints this help\n"
		   "p			Prints current results\n"
		   "a <num>		Adds result <num>\n");
}

void print_results(list_t * results) {
	int size = list_size(results);

	if(size == 0) {
		printf("There are no results to display\n");
		return;
	}

	double average = 0;

	listitem_t * current = results->first;

	while(current != NULL) {
		printf("Result: %4d\n", current->data);
		average += current->data;
		current = current->next;
	}

	average /= size;

	printf("Average result: %lf\n", average);
}
