/**
 * File:	list.c
 * Author:	Honza Remes (jan.remes@hig.no)
 * Project:	CoffeeCode
 *
 * Description: The list module file for the ADT library
 */

#include "list.h"
#include "adt.h"

#include <stdlib.h>

list_t * list_create(void) {
	list_t * retval = malloc(sizeof(list_t));

	if(retval == NULL) {
		return NULL;
		//@TODO: Record EMALLOC to library last error
	}

	retval->first = NULL;
	retval->last = NULL;
	retval->size = 0;

	return retval;
}

void list_destroy(list_t * l) {
	if(l == NULL) {
		return;		// Destroying NULL is not considered an error
	}
	list_clear(l);
	free(l);
}

int list_clear(list_t * l) {
	if(l == NULL) {
		return ENULL;	// Clearing NULL list IS an error
	}
	if(l->size == 0) {
		return EOK;		// Clearing empty list is OK
	}

	listitem_t * current = l->first;

	while(current->next != NULL) {
		listitem_t * tmp = current->next;
		free(current);
		current = tmp;
	}

	free(current);

	l->first = NULL;
	l->last = NULL;
	l->size = 0;

	return EOK;
}

int list_append(list_t *l, listdata_t item) {
	if(l == NULL) {
		return ENULL;
	}

	listitem_t * newItem = malloc(sizeof(listitem_t));
	if(newItem == NULL) {
		return EMALLOC;
	}

	newItem->next = NULL;
	newItem->prev = NULL;
	newItem->data = item;

	if(l->size == 0) {
		l->first = newItem;
		l->last = newItem;
		l->size = 1;
	} else {
		l->last->next = newItem;
		newItem->prev = l->last;
		l->size++;
	}

	return EOK;
}

listitem_t * list_search(list_t *l, listdata_t item) {
	if(l == NULL) {
		return NULL;
		//@TODO: Record ENULL to library last error
	}

	listitem_t * current = l->first;
	while(current != NULL) {
		if(current->data == item) {
			return current;
		}
		current = current->next;
	}

	return NULL;
}

int list_remove(list_t *l, listdata_t item) {
	if(l == NULL) {
		return ENULL;
	}

	listitem_t * found = list_search(l, item);
	if(found != NULL) {
		if(found == l->first) {
			l->first = found->next;
		} else {
			found->prev->next = found->next;
		}

		if(found == l->last) {
			l->last = found->prev;
		} else {
			found->next->prev = found->prev;
		}

		l->size--;
	}

	return EOK;
}

int list_size(list_t * l) {
	if(l == NULL) {
		return -1;	// @TODO: Record ENULL into library last error
	}

	return l->size;
}
