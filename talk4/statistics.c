#include "statistics.h"

int maximum(int arr[], int size) {
    int result = arr[0];

    for(int i = 0; i < size; i++) {
        if(arr[i] > result) {
            result = arr[i];
        }
    }

    return result;
}

double average(int arr[], int size) {
    double result = 0.0f;

    for(int i = 0; i < size; i++) {
        result += arr[i];
    }

    result /= size;

    return result;
}
