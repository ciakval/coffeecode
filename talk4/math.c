#include "math.h"

double absolute(double x) {
    if(x < 0) {
        return -x;
    } else {
        return x;
    }
}

int isEven(int x) {
    return ! (x % 2);
}

int fact(int x) {
    if(x < 0) {
        return -1;
    } else if(x < 3) {
        return x;
    } else {
        return x * fact(x - 1);
    }
}
