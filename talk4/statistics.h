#ifndef STATISTICS_H
#define STATISTICS_H

int maximum(int arr[], int size);

double average(int arr[], int size);

#endif // STATISTICS_H
