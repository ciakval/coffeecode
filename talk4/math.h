#ifndef MATH_H
#define MATH_H

double absolute(double x);

int isEven(int x);

int fact(int x);


#endif // MATH_H
