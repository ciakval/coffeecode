#include <stdio.h>

#include "math.h"
#include "statistics.h"

int main(void) {
    int x = -3;

    int y = absolute(x);


    printf("---- MATH ----\n");

    printf("Abs of %d is %d\n", x, y);

    int z = fact(x);

    printf("Factorial of %d is %d\n", x, z);

    printf("Number %d %s even\n", x, isEven(x) ? "is" : "is not");


    int arr[] = { 3,5,11,27,4,-1,10 };
    int size = sizeof(arr) / sizeof(int);

    printf("\n---- STATISTICS ----\n");

    printf("Array: [");
    for(int i = 0; i < size; i++) {
        printf("%d ", arr[i]);
    }
    printf("]\n");

    printf("Maximum of the array is %d\n", maximum(arr, size));
    printf("Average of the array is %lf\n", average(arr, size));


    return 0;
}
