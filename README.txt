This is the public Git repository for the CoffeeCode event series at the Gjovik
University College.

The files stored here are for the educational purposes. You may use them for
those at will.

The files may not form a complete project or further information may be required
for running them; usually, though, at least a Makefile for building the files is
present.

Ask questions at jan.remes@hig.no. 
