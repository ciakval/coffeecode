#!/usr/bin/env python3

# File:     talk7/geometry.py
# Author:   Honza Remes (jan.remes@hig.no)
# Project:  CoffeeCode

# Description: Example Python file explaining 'self' and class methods

class Circle:
    '''This class represents circles'''

    def __init__(self, d, x, y):
        self.d = d
        self.r = d / 2

        self.x = x
        self.y = y

    def print(self):
        print('This is Circle located at [' + str(self.x) + ',' + str(self.y) + '] with radius ' + str(self.r))

    def move(self, newx, newy):
        self.x = newx
        self.y = newy

class Ellipse:

    def __init__(this, x, y):
        this.x = x
        this.y = y

    def print(self):
        print('This is Ellipse located at [' + str(self.x) + ',' + str(self.y) + ']')


e1 = Ellipse(20, 30)
e1.print()

#def move(c, newx, newy):
#    c.x = newx
#    c.y = newy


c1 = Circle(10, 20, 30)
c1.print()

c1.move(10, 15)
c1.print()

#e1.move(0, 5)
Circle.move(e1, 0, 5)
e1.print()
