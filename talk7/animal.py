#!/usr/bin/env python3

# File:     talk7/animal.py
# Author:   Honza Remes (jan.remes@hig.no)
# Project:  CoffeeCode

# Description: Example Python code explaining duck-typing

class Dog:

    def __init__(self):
        weight = 10
        age = 15

    def bark(self):
        print('Bark!')

    def move(self):
        print('Going forward')


class Bus:

    def move(self):
        print('Started engine and going forward')


def try_moving(obj):
    obj.move()

def try_barking(obj, test):
    if test == True:
        obj.bark()
    else:
        print('No barking today')


d = Dog()

b = Bus()

try_moving(d)
try_moving(b)

try_barking(d, True)
try_barking(b, False)
