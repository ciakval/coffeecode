<?php
# File:     functions.php
# Author:   Jan Remeš (jan.remes@hig.no)
# Project:  Coffee & Code
#
# Description:  Provides functions for C&C website


function print_header($title) {
    echo "<!DOCTYPE html>\n";
    echo "<html>\n";
    echo "  <head>\n";
    echo "    <title>".htmlspecialchars($title)."</title>\n";
    echo "    <meta charset=\"utf-8\" />\n";
    echo "    <meta name=\"author\" content=\"Honza Remeš\" />\n";
    echo "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n";
    echo "    <link rel=\"stylesheet\" type=\"text/css\" href=\"/stylesheet.css\" />\n";
    echo "  </head>\n";
    echo "  <body>\n";
    echo "    <div id=\"global\">\n";
}

function print_navigation($root) {
    if ($root) {
        $up = "../index.php";
    } else {
        $up = "./index.php";
    }
    echo "<div id=\"navigation\">\n";
    echo "  <span id=\"navigation\"><a href=\"/\">Home</a> | <a href=\"".$up."\">Up</a></span>\n";
    echo "</div>\n";
    echo "<hr />\n";
}

function print_footer() {
    echo "    </div>\n";
    echo "  </body>\n";
    echo "</html>\n";
}

?>
