<?php
    require_once('../functions.php');
    print_header('Coffee & Code - Instructions');
    print_navigation(True);
?>

<h1>Instructions</h1>
<p>The pages here contain instructions for CC talks. They are to be read and
followed <b>in advance</b></p>

<ul>
    <li><a href="01_getlinux.php">Talk 1: Get Linux!</a></li>
</ul>

<?php print_footer(); ?>
