<?php
    require_once('../functions.php');
    print_header('Coffee & Code - Get Linux');
    print_navigation(False);
?>

<h1>Get Linux!</h1>
<p>We will be working with Linux operating system (OS). In order to have the tools
available, get your own Linux system. <i>I mean, you can get around by using
    HiG's servers instead, but you should really try to get your own.</i> This
page will explain how.</p>

<p><b>Linux is available for free, you don't have to pay anything, get/crack
    CD-keys or anything like that.</b></p>

<h2>Where will your Linux run?</h2>
There are several options where to run your Linux OS (ordered by level of
interference with your current system):
<ol>
    <li>Run "live distribution" from a flash drive</li>
    <li>Run Linux in a virtual computer</li>
    <li>Configure dual-boot on your computer</li>
    <li>Replace your current system with Linux</li>
</ol>

<p>Now, option 1 is OK for trying how Linux looks like but not for a serious
work, so not good for us.</p>

<p>Option 4 is OK if you have a spare laptop for this or if you want to stop
using Windows completely - probably not the way to go too.</p>

<p>Options 2 and 3 are both pretty good. However, setting up a virtual computer
is simpler and cannot break your current system, so I will stick with it. It has
worse performance and you might run into problems with network and/or graphics,
but this won't trouble us greatly.</p>

<p>That said, I totally encourage anyone who feels like it to go and configure
dual-boot and run Linux on their bare metal. The process is not very
complicated, but requires some knowledge about hard drive partitioning and may
cause you a data loss if done wrong, so I recommend it to those who aren't
afraid. <i>I will gladly provide advice on the topic personally</i>.</p>

<h2>Virtual Machine</h2>
<p>This is the easier and probably more user-friendly option. You can use a
virtualization software to SIMULATE a computer (guest) inside your computer
(host) and then run Linux on the guest while running Windows on the host. For
the Windows system, the guest is nothing more than another program</p>

Pros:
<ul>
    <li>Easy to set-up</li>
    <li>Transferrable to another computer</li>
    <li>CANNOT BREAK YOUR WINDOWS</li>
</ul>

Cons:
<ul>
    <li>Does not have direct access to your hardware</li>
    <li>Lower performance</li>
    <li>Graphics acceleration (OpenGL and stuff) may not work</li>
    <li>May have networking problems</li>
</ul>

How-To:
<p>First, you need to install a virtualization software (a hypervisor, if you
want to use virtualization terminology). I have very good experience using
<a href="http://virtualbox.org">VirtualBox</a>. Download and install the latest
version and also the Extension Pack (not necessary but helpful).</p>

<p>Once you have VirtualBox installed, run it and click the New button. This
will start a wizard for you to configure your virtual computer properties.
Choose whatever name you like and the Linux distribution you will use (see
below). <b>At the hard drive settings, give it at least 20 GB, or you might run
into trouble.</b> You can keep the rest of the settings as they are (although
giving the guest more memory and more CPUs can speed things up - don't be afraid
to try and experiment a little).</p>

<h2>Which Linux?</h2>
<p>Linux comes in many different forms, called <i>distributions</i>. Each
distribution contains the Linux <a href="www.kernel.org">kernel</a> and they
share many core programs and libraries, but there are a lot of programs, which
are distribution-specific. While any modern distribution will suit our needs, I
recommend to start with <a href="www.ubuntu.com">Ubuntu</a>. Get their 
<a href="http://www.ubuntu.com/download/desktop/thank-you?country=NO&version=14.04.3&architecture=amd64">ISO</a>
or find another distribution's ISO and download that. Use the name of the
selected distribution in the VirtualBox "new computer" wizard drop-down
list.</p>

<h2>Linux installation</h2>
<p>Run your new virtual computer. After it starts, it will ask for an ISO to
install your OS from. Provide it with the ISO downloaded in the previous
step. It starts in Live Mode and provides you with the option to "Install
Ubuntu". Go with it. <i>Please, install your Linux in English. It is generally
    much beter to stick with English everywhere around programming, because
    everybody will understand your variable names, error messages and will be
    able to troubleshoot your system. </i>Let's go click through the
installation.</p>

<p>Click Next, optionally, check to install updates during installation and
proceed. You can be safe with default "Erase disk and install Ubuntu" since we
are in virtual computer, remember? This computer's "disk" is just a file in your
Windows (by default <i>Home Folder/VirtualBox VMs/&lt;Virtual Computer
    Name&gt;/&lt;Virtual Computer Name&gt;.vdi</i>). Double-confirm the disk
erase and begin the installation.</p>

<p>While the installation is running, provide the system with required
information and create your user account. For my virtual computers, I usually
set up a password-less login, since it is protected by my host password, but
that's up to you. When the installation is finished, click "Restart Now". Look
carefully at the screen and press ENTER when it asks you to (VirtualBox has
already "ejected" your ISO out of the virtual drive).</p>

<p><b>Congratulations! You have installed a Linux operating system on your
    machine</b></p>

<?php print_footer(); ?>
