<?php
    require_once('../functions.php');
    print_header('Coffee & Code Talk 1');
    print_navigation(False);
?>

<h1>Talk 1: Basic Linux Commands</h1>

Outline:
<ul>
    <li>Linux / Windows differences (file system, software management)</li>
    <li>Obtaining tools (see link)</li>
    <li>File system - navigation, commands (pwd, ls, cd, mkdir, cp, mv, rm)</li>
    <li>File System Hierarchy (see link)</li>
    <li class="break">Break</li>
    <li>Terminal, shell, bash</li>
    <li>Other shells (csh, ksh, sh)</li>
    <li>File System - /; /home/&lt;USER&gt;; /etc;
</ul>

Links:
<ul>
    <li><a href="https://en.wikipedia.org/wiki/Filesystem_Hierarchy_Standard">File System Hierarchy</a></li>
    <li><a href="../resources/programs.php">Important Programs</a></li>
</ul>

<?php print_footer(); ?>
