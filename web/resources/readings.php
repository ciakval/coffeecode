<?php
require_once('../functions.php');

print_header('Coffee & Code - Readings');
print_navigation(False);
?>

<h1>Readings</h1>

<hr />
<h2>C programming language (Kernighan)</h2>
<p>Available in the HiG library, this book is perfect for those who want to
learn the C programming language. Well suited for the 1<sup>st</sup> year
students, not yet spoiled with higher-level languages like Visual C++.</p>
<a
href="http://bibsys-primo.hosted.exlibrisgroup.com/primo_library/libweb/action/dlDisplay.do?docId=BIBSYS_ILS881150681&vid=HIG">Library
link</a><br />

<a href="http://www.iups.org/media/meeting_minutes/C.pdf">PDF link</a>

<hr />

<? print_footer(); ?>
