<?php
    require_once('../functions.php');
    print_header('Coffee & Code - Programs');
    print_navigation(False);
?>
<h1>Required Programs</h1>
<p>This page lists programs which you must have on your system</p>
<ul>
    <li>bash</li>
    <li>vim</li>
    <li>gcc</li>
    <li>g++</li>
    <li>gdb</li>
    <li>make</li>
    <li>git</li>
    <li>valgrind</li>
    <li>ssh</li>
    <li>ssh-keygen</li>
</ul>

<p>Here are programs I consider useful, but not necessary for us:</p>
<ul>
    <li>cmake</li>
    <li>terminator</li>
    <li>qtcreator</li>
    <li>codeblocks</li>
</ul>

<?php print_footer(); ?>
