<?php
    require_once('../functions.php');
    print_header('Coffee & Code - Resources');
    print_navigation(True);
?>
<h1>Coffee &amp; Code - Resources</h1>
<p>Pages listed here describe important resources for our sessions</p>

<ul>
    <li><a href="programs.php">Important programs</a></li>
    <li><a href="readings.php">Books to read</a></li>
</ul>

<p>Here are links to some interesting sources of information</p>
<ul>
    <li><a href="https://wiki.archlinux.org/index.php/SSH_keys">SSH keys article</a></li>
    <li><a href="https://git-scm.com/book/en/v2/Getting-Started-Git-Basics">Git Basics</a> (whole book is probably worth reading)</li>
</ul>

<?php print_footer(); ?>
