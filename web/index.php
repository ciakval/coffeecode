<?php
    require_once('functions.php');
    print_header('Coffee & Code');
    print_navigation(False);
?>

<div id="main">

<h1>Coffee, Code &amp; Linux</h1>
<p>Welcome to the Coffee&amp;Code webpage. It is here to provide you with information
about the Coffee&amp;Code sessions.</p>

<h2>About Coffe&amp;Code</h2>
<p>Coffee&amp;Code is a "session" series where I talk about the Linux operating
system, the tools used there and about programming in general (especially in the
C programming language).</p>

<p>The events are under the patronage of Simon McCallum.</p>

<p><strong>If you would like to participate, please fill in this
<a href="http://goo.gl/forms/B1mMLOI2m4" target="_blank">form</a>.</strong></p>

<p>Here are the links for session slides</p>
<ul>
    <li><a href="slides/01-Basics.pdf">Session 1: Linux basics</a></li>
    <li><a href="slides/02-GCC,Make,Git.pdf">Session 2: GCC, Make, Git</a></li>
</ul>

Links:
<ul>
    <li><a href="instructions">Instructions (do @home)</a></li>
    <li><a href="resources">Resources (links, pages, data, etc.)</a></li>
</ul>

<h2>About me</h2>
<div id="imgbox">
    <img src="remes_web.jpg" align="center" />
</div>

<p>My name is Honza Remeš. I am a Master programme student at the Faculty of
Information Technology (<a href="http://www.fit.vutbr.cz/.en">FIT</a>) in Brno,
Czech Republic; now studying at Gjøvik for one year as my Erasmus+ exchange
programme.</p>

<p>At FIT, we have been led to use Linux for programming tasks. I consider it
the superior OS for programming, since the system is suited for command-line
control, comes with many great programmer tools and is generally suited for
programmers.</p>

<p>It is my belief, that everyone working in IT should have a solid knowledge
about Linux itself, managing the system and using the tools (<i>make, vim, git</i>)
that make Linux programmer's life easier.</p>

<p>I will gladly provide advice on Linux and system administration, as well as
on the tools (especially Git).</p>

<span>You can reach me (priority ordered):</span>
<ul>
    <li>at the school <a href="mailto: jan.remes@hig.no">mail</a>,</li>
    <li>on IRC (IRCnet, #coffeecode channel, nick 'ciakval'),</li>
    <li>on Skype (nick 'ciakval'),</li>
    <li>or on Facebook.</li>
</ul>

</div>
<?php print_footer(); ?>
